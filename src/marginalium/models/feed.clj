(ns marginalium.models.feed
  "defines a set of methods that can be used with `Feed` object"
  (:require [marginalium.models.news :as news]))

(defn present
  "present feed object."
  [feed]
  (->> feed
    (sort-by :published-date)
    (reverse)
    (map news/present)))

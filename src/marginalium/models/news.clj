(ns marginalium.models.news
  "defines a set of methods that can be used with `News` object"
  )

(defn present
  "present news object."
  [event]
  {:description    (get-in event [:description :value])
   :published_at   (get-in event [:published-date]) })
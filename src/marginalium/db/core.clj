(ns marginalium.db.core
  "define set of methods for working with feeds model in DB"
  (:use korma.core
        [korma.db :only (defdb)])
  (:require [marginalium.db.schema :as schema]))

(defdb db schema/db-spec)

(defentity feeds)

(defn save-feed
  "saves link into DB with timestamp"
  [link]
  (insert feeds
          (values {:link link
                   :timestamp (new java.util.Date)})))

(defn get-feeds
  "retriving feeds links form DB."
  []
  (select feeds))

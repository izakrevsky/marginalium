(ns marginalium.db.schema
  "defines set of methods for work with DB"
  (:require [clojure.java.jdbc :as sql]
            [noir.io :as io]))

(def db-store "site.db")

(def db-spec {:classname "org.h2.Driver"
              :subprotocol "h2"
              :subname (str (io/resource-path) db-store)
              :user "sa"
              :password ""
              :make-pool? true
              :naming {:keys clojure.string/lower-case
                       :fields clojure.string/upper-case}})
(defn initialized?
  "checks to see if the database schema is present"
  []
  (.exists (new java.io.File (str (io/resource-path) db-store ".mv.db"))))

(defn create-feeds-table
  "generate default feeds table"
  []
  (sql/db-do-commands
    db-spec
       (sql/create-table-ddl
        :feeds
        [:id "INTEGER PRIMARY KEY AUTO_INCREMENT"]
        [:timestamp :timestamp]
        [:link "varchar(300)"])))

(defn create-tables
  "creates the database tables used by the application"
  []
  (create-feeds-table))

(ns marginalium.routes.home
  "define set of methods for representing feeds and news on home page"
  (:use feedparser-clj.core)
  (:use selmer.parser)

  (:require [clojure.contrib.string :as string]
            [compojure.core :refer :all]
            [marginalium.layout :as layout]
            [marginalium.db.core :as db]
            [marginalium.models.feed :as feed]))


(defn messages
  "retriving feeds, that parses and present it"
  []
  (->>  (db/get-feeds)
        (map :link)
        (map parse-feed)
        (mapcat :entries)
        (feed/present)))

(defn links
  "retriving links"
  []
  (->>  (db/get-feeds)
        (map :link)))


(defn feeds-content
  "rendering feeds with news and feeds"
  []
  (render-file "templates/content.html", {
     :messages (messages)
     :links    (links)}))

(defn home-page
  "render layout home"
  [& [error]]
  (layout/render "home.html"
    {:error    error
     :content  (feeds-content)}))

(defn remote-save-link
  "saves link passed by AJAX"
  [params]
  (def feed-link (:link params))
  (try
    (def new-feed (parse-feed feed-link))
    (db/save-feed feed-link)
    { :body { :news (feeds-content)} }
  (catch Exception e
    { :status 500, :body  "You need to provide correct link." } )))

(defroutes home-routes
  (GET "/" [] (home-page))
  (POST "/" [link] (remote-save-link link))
  (POST "/new_feed/" {params :params} (remote-save-link params))
)


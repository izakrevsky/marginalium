$(function() {
  $("#feed-form").submit(function() {
    var url = "/new_feed/";
    $.ajax({
      type: "POST",
      url: url,
      data: $("#feed-form").serialize(),
      success: function(data) {
        $('#content').html(data.news);
      },
      error: function(xhr, status, error) {
        alert(xhr.responseText);
      }
    });
    return false;
  });
});
